from scraper.pages import spells


def test_get():
    spell_list = list(spells.get())

    names = [spell.name for spell in spell_list]
    assert "acid arrow" in names
    assert "maze" in names
    assert "zone of truth" in names

    assert "mass (spell name)" not in names


def test_get_spell_list():
    list = spells.get_spell_list()
    names = [spell[0] for spell in list]
    assert "Acid Arrow" in names
    assert "Insect Plague" in names
    assert "Zone of Truth" in names


def test_get_range_attack_spell():
    result = spells.get_spell("http://paizo.com/pathfinderRPG/prd/coreRulebook/spells/acidArrow.html#acid-arrow")

    assert result.name == "acid arrow"
    assert result.school == "conjuration (creation) [acid]"
    assert result.level == "sorcerer/wizard 2"
    assert result.casting_time == "1 standard action"
    assert result.components == "V, S, M (rhubarb leaf and an adder's stomach), F (a dart)"
    assert result.range == "long (400 ft. + 40 ft./level)"
    assert result.effect == "one arrow of acid"
    assert result.duration == "1 round + 1 round per three levels"
    assert result.saving_throw == "none"
    assert result.spell_resistance == "no"
    assert result.description.startswith("An arrow of acid")
    assert result.description.endswith("damage in each round.")


def test_get_touch_spell():
    result = spells.get_spell("http://paizo.com/pathfinderRPG/prd/coreRulebook/spells/aid.html#aid")

    assert result.name == "aid"
    assert result.school == "enchantment (compulsion) [mind-affecting]"
    assert result.level == "cleric 2"
    assert result.casting_time == "1 standard action"
    assert result.components == "V, S, DF"
    assert result.range == "touch"
    assert result.target == "living creature touched"
    assert result.duration == "1 min./level"
    assert result.saving_throw == "none"
    assert result.spell_resistance == "yes (harmless)"


def test_puny_spell_with_no_details():
    result = spells.get_spell(
        "http://paizo.com/pathfinderRPG/prd/coreRulebook/spells/callLightningStorm.html#call-lightning-storm")

    assert result.name == "call lightning storm"
    assert result.school == "evocation [electricity]"
    assert result.level == "druid 5"
    assert result.range == "long (400 ft. + 40 ft./level)"
    assert result.description.startswith("This spell functions")
    assert result.description.endswith("of 15 bolts.")


def test_stupid_page_that_decides_to_use_strong_as_a_tag_instead_of_b_like_all_the_normal_pages():
    result = spells.get_spell("http://paizo.com/pathfinderRPG/prd/coreRulebook/spells/makeWhole.html#make-whole")

    assert result.name == "make whole"
    assert result.school == "transmutation"
    assert result.level == "cleric 2, sorcerer/wizard 2"
    assert result.range == "close (25 ft. + 5 ft./2 levels)"
    assert result.target == "one object of up to 10 cu. ft./level or one construct creature of any size"
    assert result.description.startswith("This spell functions")
    assert result.description.endswith("allow spell resistance.")


def test_multiline_description():
    result = spells.get_spell("http://paizo.com/pathfinderRPG/prd/coreRulebook/spells/alterSelf.html#alter-self")

    assert result.description.startswith("When you cast")
    assert result.description.endswith("to your Strength.")

# TODO handle alternate versions of spells
# e.g. http://paizo.com/pathfinderRPG/prd/coreRulebook/spells/bearSEndurance.html#bear-s-endurance