from typing import List, Tuple, Generator, Any

from scraper.parser import get_page_content
from scraper import util, Table
from bs4 import BeautifulSoup

BASE_URL = "http://paizo.com/pathfinderRPG/prd/coreRulebook"
SPELL_LIST_URL = f"{BASE_URL}/spellindex.html"


class Spell:
    def __init__(self, name):
        self.name = name
        self.school = None
        self.level = None
        self.casting_time = None
        self.components = None
        self.range = None
        self.target = None
        self.effect = None
        self.duration = None
        self.saving_throw = None
        self.spell_resistance = None
        self.description = None

    def as_list(self) -> List[str]:
        return [
            self.name,
            self.school,
            self.level,
            self.casting_time,
            self.components,
            self.range,
            self.target,
            self.effect,
            self.duration,
            self.saving_throw,
            self.spell_resistance,
            self.description
        ]


# spells to be ignored from big list
excluded = [
    # Not actual spells
    "Greater (Spell Name)",
    "Lesser (Spell Name)",
    "Mass (Spell Name)"
]


def get_as_table() -> Table:
    spells = get()

    header = ["name", "school", "level", "casting_time", "components", "range", "target", "effect", "duration",
              "saving_throw", "spell_resistance", "description"]
    rows = [spell.as_list() for spell in spells]
    return Table("Spells", header, rows)


def get() -> Generator[List[Spell], None, None]:
    for spell in get_spell_list():
        name = spell[0]
        if name in excluded:
            continue
        path = spell[1]
        print(f"Getting spell: {name}")
        yield get_spell(f"{BASE_URL}/{path}")


def get_spell_list() -> List[str]:
    content = get_page_content(SPELL_LIST_URL, main_class="body")
    lists = content.find_all("ul")
    return util.flatten_list([ul_to_list(ul) for ul in lists])


def ul_to_list(ul) -> list:
    children = ul.find_all("li")
    return [process_li(li) for li in children]


def process_li(li) -> Tuple[str, str]:
    name = li.next.text
    link = li.next.get("href")
    return name, link


def get_siblings_until(soup: BeautifulSoup, test: List[str]) -> str:
    siblings = []
    for element in soup.next_siblings:
        if element.name in test:
            break
        else:
            if element.string:
                siblings.append(element.string)
    return "".join(siblings)


def split_by_bold(soup: BeautifulSoup) -> Generator[Tuple[str, str], Any, None]:
    tags = ["b", "strong"]
    for tag in soup:
        if tag.name in tags:
            key = tag.text
            value = get_siblings_until(tag, tags)
            yield key, format_text(value)


def format_text(text: str):
    """remove unneeded characters from string

    removed:
        ending semi-colon
        whitespace at start and end
    """

    return text.strip().rstrip(";")


def get_description(page_paragraphs: List[BeautifulSoup]) -> str:
    spell_paragraphs = [
                           p for p in page_paragraphs
                           if p.get("class") is None
                       ][:-1]  # TODO test if last paragraph is always report problem
    description = "\n".join([p.text for p in spell_paragraphs])
    return description


def get_spell(url: str):
    page = get_page_content(url, main_class="body")
    paragraphs = page.find_all("p")
    description = get_description(paragraphs)

    sections = util.flatten_list(
        [split_by_bold(paragraph) for paragraph in paragraphs]
    )
    sections_dict = {s[0].lower().strip(): s[1] for s in sections}

    name = sections[0][0].lower().strip()

    # Add all parameters to class
    # get is used if parameter is optional
    spell = Spell(name)
    spell.school = sections_dict["school"]
    spell.level = sections_dict["level"]
    spell.casting_time = sections_dict.get("casting time")
    spell.components = sections_dict.get("components")
    spell.target = sections_dict.get("target")
    spell.range = sections_dict.get("range")
    spell.effect = sections_dict.get("effect")
    spell.duration = sections_dict.get("duration")
    spell.saving_throw = sections_dict.get("saving throw")
    spell.spell_resistance = sections_dict.get("spell resistance")
    spell.description = description

    return spell
