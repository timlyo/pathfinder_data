import csv
import pprint
from typing import List

import requests_cache
import argparse

from scraper.parser import Table

requests_cache.install_cache("requests_cache")
pprint = pprint.PrettyPrinter().pprint

from scraper import pages

argparser = argparse.ArgumentParser(description="Get pathfinder data")
argparser.add_argument("--format", choices=["json", "csv"], default="csv")


def get_all_datasets() -> List[Table]:
    return pages.equipment.get() + [pages.spells.get_as_table()]


def write_to_csv(data: List[Table]):
    for table in data:
        with open(f"../data/{table.title}.csv", "w") as file:
            writer = csv.writer(file)
            writer.writerow(table.headers)
            for row in table.rows:
                writer.writerow(row)


if __name__ == "__main__":
    args = argparser.parse_args()
    print(args)
    data = get_all_datasets()

    output_format = args.format
    if output_format == "csv":
        write_to_csv(data)
